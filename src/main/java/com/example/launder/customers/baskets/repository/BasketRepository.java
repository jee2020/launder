package com.example.launder.customers.baskets.repository;

import com.example.launder.customers.baskets.models.Basket;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface BasketRepository extends MongoRepository<Basket, String> {
    @Query("{ current :  true, userId : ?0 }")
    Basket findByUserId(String userId);

    @Query("{ current : false }")
    List<Basket> findAllByUserIdOrderByCreatedTimestampDesc(String userId);
}
