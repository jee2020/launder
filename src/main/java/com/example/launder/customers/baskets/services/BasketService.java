package com.example.launder.customers.baskets.services;


import com.example.launder.customers.baskets.DTO.BasketDTO;
import com.example.launder.customers.baskets.models.Basket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BasketService {

    @Value("${app.basket.content.weight}")
    private Integer weight;

    public Basket createBasket(String userId){
        Basket basket = new Basket();
        basket.setWeight(0);
        basket.setUserId(userId);
        basket.setCurrent(true);
        basket.setCreatedTimestamp(new Date().getTime());
        return basket;
    }

    public Integer calculateBasketPrice(Basket basket){
        Integer price = 0;
        price = basket.getWeight() * this.weight;
        return price;
    }


    public BasketDTO BasketToDTO(Basket basket){
        BasketDTO basketDTO = new BasketDTO();
        basketDTO.set_id(basket.get_id());
        basketDTO.setCurrent(basket.getCurrent());
        basketDTO.setWeight(basket.getWeight());
        basketDTO.setUserId(basket.getUserId());
        basketDTO.setPrice(basket.getPrice() == null ? 0 : basket.getPrice());
        basketDTO.setCreatedTimestamp(basket.getCreatedTimestamp());
        return basketDTO;
    }
}
