package com.example.launder.customers.baskets.resources;

import com.example.launder.customers.baskets.DTO.BasketDTO;
import com.example.launder.customers.baskets.models.Basket;
import com.example.launder.customers.baskets.repository.BasketRepository;
import com.example.launder.customers.baskets.services.BasketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/baskets")
public class BasketResource {

    @Autowired
    BasketRepository basketRepository;
    private final BasketService basketService;

    public BasketResource(BasketService basketService) { this.basketService = basketService; }

    @GetMapping("/{basketId}")
    public ResponseEntity<?> getBasket(@PathVariable("basketId") String basketId){
        Optional<Basket> basket = basketRepository.findById(basketId);
        if(basket.isPresent()){
            BasketDTO basketDTO = basketService.BasketToDTO(basket.get());
            return new ResponseEntity<>(basketDTO, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/{userId}")
    public ResponseEntity<?> createBasket(@PathVariable("userId") String userId){
        Basket hasACurrentBasket = basketRepository.findByUserId(userId);
        if(hasACurrentBasket == null){
            Basket basket = basketService.createBasket(userId);
            basketRepository.save(basket);
            basket.set_id(basket.get_id());
            return new ResponseEntity<>(basketService.BasketToDTO(basket), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(basketService.BasketToDTO(hasACurrentBasket), HttpStatus.ALREADY_REPORTED);
        }
    }

    @GetMapping("/current/{userId}")
    public ResponseEntity<?> getCurrentBasket(@PathVariable("userId") String userId){
        Basket basket = basketRepository.findByUserId(userId);
        basket.setPrice(basketService.calculateBasketPrice(basket));
        return new ResponseEntity<>(basketService.BasketToDTO(basket), HttpStatus.OK);
    }

    @GetMapping("/history/{userId}")
    public ResponseEntity<?> getBasketsHistory(@PathVariable("userId") String userId){
        List<Basket> basketsHistory = basketRepository.findAllByUserIdOrderByCreatedTimestampDesc(userId);
        for(int i = 0; i < basketsHistory.size(); i++) basketsHistory.get(i).setPrice(basketService.calculateBasketPrice(basketsHistory.get(i)));
        return new ResponseEntity<>(basketsHistory, HttpStatus.OK);
    }

    @PutMapping("/{basketId}")
    public ResponseEntity<?> updateBasketWeight(@PathVariable("basketId") String basketId, @RequestBody Integer weight){
        Optional<Basket> basket = basketRepository.findById(basketId);
        if(basket.isPresent()){
            basket.get().setWeight(weight);
            basketRepository.save(basket.get());
            return new ResponseEntity<>(basketService.BasketToDTO(basket.get()), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

}
