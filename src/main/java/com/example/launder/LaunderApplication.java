package com.example.launder;

import com.example.launder.customers.baskets.repository.BasketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LaunderApplication {

    public static void main(String[] args) {
        SpringApplication.run(LaunderApplication.class, args);
    }

}
